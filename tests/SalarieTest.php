<?php

use Dev\Tp102\Salarie;
use PHPUnit\Framework\TestCase;

class SalarieTest extends TestCase
{
    /**
    * @covers Carre::setMatricule
    */
    public function testSetMatricule()
    {
        $salarie = new Salarie();
        $salarie->setMatricule(123456);
        $this->assertEquals(123456, $salarie->getMatricule());
    }
    /**
    * @covers Carre::GetDateEmbauche
    */
    public function testGetDateEmbauche()
    {
        $dateEmbauche = new DateTime("2022-01-01");
        $salarie = new Salarie(123, "John Doe", 2000, $dateEmbauche);
        $this->assertEquals($dateEmbauche, $salarie->getDateEmbauche());
    }
    /**
    * @covers Carre::Experience
    */
    public function testExperience()
    {
        $dateEmbauche = new DateTime("2018-06-01");
        $salarie = new Salarie(123, "John Doe", 2000, $dateEmbauche);
        $this->assertEquals(5, $salarie->experience());
    }
    /**
    * @covers Carre::CalculerSalaireNet
    */
    public function testCalculerSalaireNet()
    {
        $salarie = new Salarie(123, "John Doe", 2000);
        $salaireNet = $salarie->calculerSalaireNet();
        $this->assertEquals(1600, $salaireNet);
    }
    /**
    * @covers Carre::PrimeAnnuelle
    */
    public function testPrimeAnnuelle()
    {
        $dateEmbauche = new DateTime("2018-01-01");
        $salarie = new Salarie(123, "John Doe", 2000, $dateEmbauche);
        $primeAnnuelle = $salarie->primeAnnuelle();
        $this->assertEquals(2100, $primeAnnuelle);
    }
}
